﻿using System;
using System.Collections.Generic;
using System.Linq;
using CT.Common.DataAccess;

namespace CT.Adapter.DataAccessSQL.DAL
{
    public class LibroDAL
    {
        public List<tbl_Libro> Consultar_tbl_Libro()
        {
            List<tbl_Libro> objRetorno = null;

            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                IQueryable<tbl_Libro> objquery = (from item in Context.tbl_Libro
                                                  select item);
                if (objquery.Any())
                {
                    objRetorno = objquery.ToList();
                }
            }
            return objRetorno;
        }

        public List<tbl_Libro> Consultar_tbl_Libro_X_Nombre(String name)
        {
            List<tbl_Libro> objRetorno = null;

            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                IQueryable<tbl_Libro> objquery = (from item in Context.tbl_Libro
                                                  where item.Nombre_Libro == name
                                                  select item);
                if (objquery.Any())
                {
                    objRetorno = objquery.ToList();
                }
            }
            return objRetorno;
        }

        public Int32 Crear_tbl_Libro(tbl_Libro _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                Context.tbl_Libro.Add(_object);
                Context.SaveChanges();

                var cod_Libro = (from itemlibro in Context.tbl_Libro
                                 select itemlibro.Cod_Libro).Max();

                return cod_Libro;
            }
        }

        public void Actualizar_tbl_Libro(tbl_Libro _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                var query = (from itemlibro in Context.tbl_Libro
                             where itemlibro.Cod_Libro == _object.Cod_Libro
                             select itemlibro);
                if (query.Any())
                {
                    foreach (var item in query)
                    {
                        item.Cod_Autor = _object.Cod_Autor;
                        item.ISBN = _object.ISBN;
                        item.Nombre_Libro = _object.Nombre_Libro;
                    }
                    Context.SaveChanges();
                }
            }
        }

        public void Eliminar_tbl_Libro(int cod_Libro)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                var query = (from itemlibro in Context.tbl_Libro
                             where itemlibro.Cod_Libro == cod_Libro
                             select itemlibro).FirstOrDefault();
                if (query != null)
                {
                    Context.tbl_Libro.Remove(query);
                    Context.SaveChanges();
                }

                var query2 = (from itemLibroXCategoria in Context.tbl_LibroXCategoria
                              where itemLibroXCategoria.Cod_Libro == cod_Libro
                              select itemLibroXCategoria).FirstOrDefault();
                if (query2 != null)
                {
                    Context.tbl_LibroXCategoria.Remove(query2);
                    Context.SaveChanges();
                }
            }
        }
    }
}
