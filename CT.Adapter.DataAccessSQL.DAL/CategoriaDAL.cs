﻿using System;
using System.Collections.Generic;
using System.Linq;
using CT.Common.DataAccess;

namespace CT.Adapter.DataAccessSQL.DAL
{
    public class CategoriaDAL
    {
        public List<tbl_Categoria> Consultar_tbl_Categoria()
        {
            List<tbl_Categoria> objRetorno = null;
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                IQueryable<tbl_Categoria> objquery = (from itemCategoria in Context.tbl_Categoria
                                                      select itemCategoria);
                if (objquery.Any())
                {
                    objRetorno = objquery.ToList();
                }
            }
            return objRetorno;
        }

        public List<tbl_Libro> Consultar_tbl_Libro_X_Categoria(String name)
        {
            List<tbl_Libro> objRetorno = null;
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                IQueryable<tbl_Libro> objquery = (from itemlibro in Context.tbl_Libro
                                                  from itemLibroXCategoria in Context.tbl_LibroXCategoria
                                                  from itemCategoria in Context.tbl_Categoria
                                                  where itemlibro.Cod_Libro == itemLibroXCategoria.Cod_Libro &&
                                                  itemLibroXCategoria.Cod_Categoria == itemCategoria.Cod_Categoria &&
                                                  itemCategoria.Nombre_Categoria == name
                                                  select itemlibro);
                if (objquery.Any())
                {
                    objRetorno = objquery.ToList();
                }
            }
            return objRetorno;
        }

        public void Crear_tbl_Categoria(tbl_Categoria _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                Context.tbl_Categoria.Add(_object);
                Context.SaveChanges();
            }
        }
        
        public void Actualizar_tbl_Categoria(tbl_Categoria _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                var query = (from itemCategoria in Context.tbl_Categoria
                             where itemCategoria.Cod_Categoria == _object.Cod_Categoria
                             select itemCategoria);
                if (query.Any())
                {
                    foreach (var item in query)
                    {
                        item.Nombre_Categoria = _object.Nombre_Categoria;
                        item.Descripcion_Categoria = _object.Descripcion_Categoria;
                    }
                    Context.SaveChanges();
                }
            }
        }

        public void Eliminar_tbl_Categoria(int cod_Categoria)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                var query = (from itemCategoria in Context.tbl_Categoria
                             where itemCategoria.Cod_Categoria == cod_Categoria
                             select itemCategoria).FirstOrDefault();
                if (query != null)
                {
                    Context.tbl_Categoria.Remove(query);
                    Context.SaveChanges();
                }

                var query2 = (from itemLibroXCategoria in Context.tbl_LibroXCategoria
                              where itemLibroXCategoria.Cod_Categoria == cod_Categoria
                              select itemLibroXCategoria).FirstOrDefault();
                if (query2 != null)
                {
                    Context.tbl_LibroXCategoria.Remove(query2);
                    Context.SaveChanges();
                }
            }
        }

        public void Crear_tbl_LibroXCategoria(tbl_LibroXCategoria _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                Context.tbl_LibroXCategoria.Add(_object);
                Context.SaveChanges();
            }
        }
    }
}
