﻿using System;
using System.Collections.Generic;
using System.Linq;
using CT.Common.DataAccess;

namespace CT.Adapter.DataAccessSQL.DAL
{
    public class AutorDAL
    {
        public List<tbl_Autor> Consultar_tbl_Autor()
        {
            List<tbl_Autor> objRetorno = null;
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                IQueryable<tbl_Autor> objquery = (from itemAutor in Context.tbl_Autor
                                                  select itemAutor);
                if (objquery.Any())
                {
                    objRetorno = objquery.ToList();
                }
            }
            return objRetorno;
        }

        public List<tbl_Libro> Consultar_tbl_Libro_X_Autor(String name)
        {
            List<tbl_Libro> objRetorno = null;
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                IQueryable<tbl_Libro> objquery = (from itemlibro in Context.tbl_Libro
                                                  from itemAutor in Context.tbl_Autor
                                                  where itemlibro.Cod_Autor == itemAutor.Cod_Autor &&
                                                  itemAutor.Nombre_Autor == name
                                                  select itemlibro);
                if (objquery.Any())
                {
                    objRetorno = objquery.ToList();
                }
            }
            return objRetorno;
        }

        public void Crear_tbl_Autor(tbl_Autor _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                Context.tbl_Autor.Add(_object);
                Context.SaveChanges();
            }
        }

        public void Actualizar_tbl_Autor(tbl_Autor _object)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                var query = (from itemAutor in Context.tbl_Autor
                             where itemAutor.Cod_Autor == _object.Cod_Autor
                             select itemAutor);

                if (query.Any())
                {
                    foreach (var item in query)
                    {
                        item.Nombre_Autor = _object.Nombre_Autor;
                        item.Apellido_Autor = _object.Apellido_Autor;
                    }
                    Context.SaveChanges();
                }
            }
        }

        public void Eliminar_tbl_Autor(int cod_Autor)
        {
            using (BibliotecaEntities Context = new BibliotecaEntities())
            {
                var query = (from itemAutor in Context.tbl_Autor
                             where itemAutor.Cod_Autor == cod_Autor
                             select itemAutor).FirstOrDefault();

                if (query != null)
                {
                    Context.tbl_Autor.Remove(query);
                    Context.SaveChanges();
                }
            }
        }
    }
}
