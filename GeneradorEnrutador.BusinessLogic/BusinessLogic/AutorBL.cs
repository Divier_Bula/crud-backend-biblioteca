﻿using CT.Adapter.DataAccessSQL.DAL;
using CT.Common.DataAccess;
using CT.Common.Models;
using System;
using System.Collections.Generic;

namespace GeneradorEnrutador.BusinessLogic.BusinessLogic
{
    public class AutorBL : GeneradorOperacionBLBase
    {
        public RespuestaDTO Consultar_Autores()
        {
            try
            {
                List<tbl_Autor> _tbl_Autor = new AutorDAL().Consultar_tbl_Autor();

                List<AutorDTO> response = new List<AutorDTO>();
                if (_tbl_Autor != null)
                {
                    foreach (var origen in _tbl_Autor)
                    {
                        AutorDTO destino = new AutorDTO();
                        destino.Cod_Autor = origen.Cod_Autor;
                        destino.Nombre_Autor = origen.Nombre_Autor;
                        destino.Apellido_Autor = origen.Apellido_Autor;
                        response.Add(destino);
                    }
                }

                return new RespuestaDTO()
                {
                    Exito = true,
                    Autor = response
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };

                
            }
        }

        public RespuestaDTO Consultar_Libros_X_Autor(string name)
        {
            try
            {
                List<tbl_Libro> _tbl_Libro = new AutorDAL().Consultar_tbl_Libro_X_Autor(name);

                List<LibroDTO> response = new List<LibroDTO>();
                if (_tbl_Libro != null)
                {
                    foreach (var origen in _tbl_Libro)
                    {
                        LibroDTO destino = new LibroDTO();
                        destino.Cod_Autor = origen.Cod_Autor;
                        destino.Cod_Libro = origen.Cod_Libro;
                        destino.ISBN = origen.ISBN;
                        destino.Nombre_Libro = origen.Nombre_Libro;
                        response.Add(destino);
                    }
                }

                return new RespuestaDTO()
                {
                    Exito = true,
                    Libros = response
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Crear_Autor(String nombre_Autor, String apellido_Autor)
        {
            try
            {
                tbl_Autor _object = new tbl_Autor()
                {
                    Nombre_Autor = nombre_Autor,
                    Apellido_Autor = apellido_Autor
                };
                new AutorDAL().Crear_tbl_Autor(_object);

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Actualizar_Autor(String nombre_Autor, String apellido_Autor, string cod_autor)
        {
            try
            {
                tbl_Autor _object = new tbl_Autor()
                {
                    Cod_Autor = Convert.ToInt32(cod_autor),
                    Nombre_Autor = nombre_Autor,
                    Apellido_Autor = apellido_Autor
                };
                new AutorDAL().Actualizar_tbl_Autor(_object);

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Eliminar_Autor(string cod_Autor)
        {
            try
            {
                new AutorDAL().Eliminar_tbl_Autor(Convert.ToInt32(cod_Autor));

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }
    }
}
