﻿using CT.Adapter.DataAccessSQL.DAL;
using CT.Common.Models;
using System.Collections.Generic;
using CT.Common.DataAccess;
using System.Linq;
using System;

namespace GeneradorEnrutador.BusinessLogic.BusinessLogic
{
    public class LibroBL : GeneradorOperacionBLBase
    {
        public RespuestaDTO Consultar_Libros()
        {
            try
            {
                List<tbl_Libro> _tbl_Libro = new LibroDAL().Consultar_tbl_Libro();

                List<LibroDTO> response = new List<LibroDTO>();
                if (_tbl_Libro != null)
                {
                    foreach (var origen in _tbl_Libro)
                    {
                        LibroDTO destino = new LibroDTO();
                        destino.Cod_Autor = origen.Cod_Autor;
                        destino.Cod_Libro = origen.Cod_Libro;
                        destino.ISBN = origen.ISBN;
                        destino.Nombre_Libro = origen.Nombre_Libro;
                        response.Add(destino);
                    }
                }

                return new RespuestaDTO()
                {
                    Exito = true,
                    Libros = response
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Consultar_Libros_X_Nombre(string name)
        {
            try
            {
                List<tbl_Libro> _tbl_Libro = new LibroDAL().Consultar_tbl_Libro_X_Nombre(name);

                List<LibroDTO> response = new List<LibroDTO>();
                if (_tbl_Libro != null)
                {
                    foreach (var origen in _tbl_Libro)
                    {
                        LibroDTO destino = new LibroDTO();
                        destino.Cod_Autor = origen.Cod_Autor;
                        destino.Cod_Libro = origen.Cod_Libro;
                        destino.ISBN = origen.ISBN;
                        destino.Nombre_Libro = origen.Nombre_Libro;
                        response.Add(destino);
                    }
                }

                return new RespuestaDTO()
                {
                    Exito = true,
                    Libros = response
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Crear_Libro(string cod_Autor, string iSBN, string nombre_Libro, string cod_Categoria)
        {
            try
            {
                tbl_Libro _object = new tbl_Libro()
                {
                    ISBN = Convert.ToInt32(iSBN),
                    Nombre_Libro = nombre_Libro,
                    Cod_Autor = Convert.ToInt32(cod_Autor)
                };
                Int32 cod_Libro = new LibroDAL().Crear_tbl_Libro(_object);

                tbl_LibroXCategoria _objet2 = new tbl_LibroXCategoria()
                {
                    Cod_Libro = cod_Libro,
                    Cod_Categoria = Convert.ToInt32(cod_Categoria)
                };
                new CategoriaDAL().Crear_tbl_LibroXCategoria(_objet2);

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Actualizar_Libro(string cod_Autor, string iSBN, string nombre_Libro, string Cod_Libro)
        {
            try
            {
                tbl_Libro _object = new tbl_Libro()
                {
                    Cod_Libro = Convert.ToInt32(Cod_Libro),
                    ISBN = Convert.ToInt32(iSBN),
                    Nombre_Libro = nombre_Libro,
                    Cod_Autor = Convert.ToInt32(cod_Autor)
                };
                new LibroDAL().Actualizar_tbl_Libro(_object);

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Eliminar_Libro(string cod_Libro)
        {
            try
            {
                new LibroDAL().Eliminar_tbl_Libro(Convert.ToInt32(cod_Libro));

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

    }
}
