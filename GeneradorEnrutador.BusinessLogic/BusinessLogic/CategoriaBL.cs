﻿using CT.Adapter.DataAccessSQL.DAL;
using CT.Common.DataAccess;
using CT.Common.Models;
using System;
using System.Collections.Generic;

namespace GeneradorEnrutador.BusinessLogic.BusinessLogic
{
    public class CategoriaBL : GeneradorOperacionBLBase
    {
        public RespuestaDTO Consultar_Categorias(string name)
        {
            try
            {
                List<tbl_Categoria> _tbl_Categoria = new CategoriaDAL().Consultar_tbl_Categoria();

                List<CategoriaDTO> response = new List<CategoriaDTO>();
                if (_tbl_Categoria != null)
                {
                    foreach (var origen in _tbl_Categoria)
                    {
                        CategoriaDTO destino = new CategoriaDTO();
                        destino.Cod_Categoria = origen.Cod_Categoria;
                        destino.Nombre_Categoria = origen.Nombre_Categoria;
                        destino.Descripcion_Categoria = origen.Descripcion_Categoria;
                        response.Add(destino);
                    }
                }

                return new RespuestaDTO()
                {
                    Exito = true,
                    Categoria = response
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Consultar_Libros_X_Categoria(string name)
        {
            try
            {
                List<tbl_Libro> _tbl_Libro = new CategoriaDAL().Consultar_tbl_Libro_X_Categoria(name);

                List<LibroDTO> response = new List<LibroDTO>();
                if (_tbl_Libro != null)
                {
                    foreach (var origen in _tbl_Libro)
                    {
                        LibroDTO destino = new LibroDTO();
                        destino.Cod_Autor = origen.Cod_Autor;
                        destino.Cod_Libro = origen.Cod_Libro;
                        destino.ISBN = origen.ISBN;
                        destino.Nombre_Libro = origen.Nombre_Libro;
                        response.Add(destino);
                    }
                }

                return new RespuestaDTO()
                {
                    Exito = true,
                    Libros = response
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Crear_Categoria(string nombre_Categoria, string descripcion_Categoria)
        {
            try
            {
                tbl_Categoria _object = new tbl_Categoria()
                {

                    Nombre_Categoria = nombre_Categoria,
                    Descripcion_Categoria = descripcion_Categoria
                };
                new CategoriaDAL().Crear_tbl_Categoria(_object);

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Actualizar_Categoria(string nombre_Categoria, string descripcion_Categoria, string Cod_Categoria)
        {
            try
            {
                tbl_Categoria _object = new tbl_Categoria()
                {
                    Cod_Categoria = Convert.ToInt32(Cod_Categoria),
                    Nombre_Categoria = nombre_Categoria,
                    Descripcion_Categoria = descripcion_Categoria
                };
                new CategoriaDAL().Actualizar_tbl_Categoria(_object);

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }

        public RespuestaDTO Eliminar_Categoria(string cod_Categoria)
        {
            try
            {
                new CategoriaDAL().Eliminar_tbl_Categoria(Convert.ToInt32(cod_Categoria));

                return new RespuestaDTO()
                {
                    Exito = true
                };
            }
            catch (Exception e)
            {
                new CT.Common.Utilities.Log4Net().registrarLog(e.Message.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name.ToString());

                return new RespuestaDTO()
                {
                    Exito = false,
                    Mensaje = string.Format("Se presento un error en el sistema, {0}", e.Message).ToString()
                };
            }
        }
    }
}
