﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ConsultaProxy;
using ModelCobertura;
using Common.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Cobertura = serviceProxy.ServicioOfertaComercial;
using CrearDir = serviceProxy.ServicioUbicacion;
using Callejero = serviceProxy.WebServiceSoap;

namespace GeneradorEnrutador.BusinessLogic
{
    public class ConsultaBL : GeneradorOperacionBLBase
    {
        public modelConsultaCobertura modelo = new modelConsultaCobertura();
        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Consulta el listado de departamentos
        /// </summary>
        /// <returns></returns>
        public modelConsultaCobertura consultarDepartamento()
        {
            modelo.listDepartamento = new List<ModelDepartamentos>();
            modelo.RespuestaDTO = null;

            try
            {
                RespuestaDTO response = new FachadaDepartamento().consultarDepartamento(Servicios.url_Departamento);

                if (response.Exito)
                {
                    modelo.listDepartamento.Add(new ModelDepartamentos() { codAtisDepartamento = "-1", Departamento = "Seleccione un item" });
                    modelo.listDepartamento.AddRange((from r in XDocument.Parse(response.Mensaje).Root.Elements("Mensaje")
                                                                      select new ModelDepartamentos()
                                                                      {
                                                                          codAtisDepartamento = (string)r.Element("codAtisDepartamento"),
                                                                          Departamento = (string)r.Element("Departamento")
                                                                      }).ToList());
                }
                else
                {
                    modelo.RespuestaDTO = new RespuestaDTO()
                    {
                        Codigo = response.Codigo,
                        Descripcion = response.Descripcion,
                        Mensaje = response.Mensaje
                    };
                }
            }
            catch (Exception ex)
            {
                modelo.RespuestaDTO = new RespuestaDTO()
                {
                    Codigo = "-1",
                    Descripcion = ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""),
                    Mensaje = ""
                };

            }
            return modelo;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Consulta el Listado de Municipios
        /// </summary>
        /// <param name="codAtisDepartamento"> Codigo Atis Del Departamento</param>
        /// <returns></returns>
        public modelConsultaCobertura consultarMunicipio(string codAtisDepartamento)
        {
            modelo.listMunicipio = new List<ModelMunicipio>();
            modelo.RespuestaDTO = null;

            try
            {
                RespuestaDTO response = new FachadaMunicipio().consultarMunicipio(Servicios.url_Municipio, codAtisDepartamento);

                if (response.Exito)
                {
                    modelo.listMunicipio.Add(new ModelMunicipio() { codAtisMunicipio = "-1", nombreMunicipio = "Seleccione un item" });
                    modelo.listMunicipio.AddRange((from r in XDocument.Parse(response.Mensaje).Root.Elements("Mensaje")
                                                                   select new ModelMunicipio()
                                                                   {
                                                                       codAtisMunicipio = (string)r.Element("codAtisMunicipio"),
                                                                       codDaneMunicipio = (string)r.Element("codDaneMunicipio"),
                                                                       nombreMunicipio = (string)r.Element("Municipio")
                                                                   }).ToList());
                }
                else
                {
                    modelo.RespuestaDTO = new RespuestaDTO()
                    {
                        Codigo = response.Codigo,
                        Descripcion = response.Descripcion,
                        Mensaje = response.Mensaje
                    };
                }
            }
            catch (Exception ex)
            {
                modelo.RespuestaDTO = new RespuestaDTO()
                {
                    Codigo = "-1",
                    Descripcion = ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""),
                    Mensaje = ""
                };
            }

            return modelo;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Consulta la localidades de un municipio
        /// </summary>
        /// <param name="parametrosJson">parametrosJson del municipio</param>
        /// <returns></returns>
        public modelConsultaCobertura consultarLocalidad(string parametrosJson)
        {
            modelo.listLocalidad = new List<ModelLocalidad>();
            modelo.RespuestaDTO = null;

            try
            {
                RespuestaDTO response = new FachadaLocalidad().consultarLocalidad(Servicios.url_Localidad, parametrosJson);

                if (response.Exito)
                {
                    modelo.listLocalidad.Add(new ModelLocalidad() { codAtisLocalidad = "-1", nomLocalidad = "Seleccione un item" });

                    JToken jsonObject = JToken.Parse(response.Mensaje);
                    if (jsonObject.Type == JTokenType.Object)
                        modelo.listLocalidad.Add(JsonConvert.DeserializeObject<ModelLocalidad>(response.Mensaje));
                    else if (jsonObject.Type == JTokenType.Array)
                        modelo.listLocalidad.AddRange(JsonConvert.DeserializeObject<List<ModelLocalidad>>(response.Mensaje));
                }
                else
                {
                    modelo.RespuestaDTO = new RespuestaDTO()
                    {
                        Codigo = response.Codigo,
                        Descripcion = response.Descripcion,
                        Mensaje = response.Mensaje
                    };
                }
            }
            catch (Exception ex)
            {
                modelo.RespuestaDTO = new RespuestaDTO()
                {
                    Codigo = "-1",
                    Descripcion = ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""),
                    Mensaje = ""
                };
            }
            return modelo;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Consulta el listado de barrios
        /// </summary>
        /// <param name="parametrosJson"> parametrosJson del Barrio</param>
        /// <returns></returns>
        public modelConsultaCobertura consultarBarrio(string parametrosJson)
        {
            modelo.listBarrio = new List<ModelBarrio>();
            modelo.RespuestaDTO = null;

            try
            {
                RespuestaDTO response = new FachadaBarrio().consultarBarrio(Servicios.url_Barrio, parametrosJson);

                if (response.Exito)
                {
                    JToken jsonObject = JToken.Parse(response.Mensaje);
                    if (jsonObject.Type == JTokenType.Object)
                        modelo.listBarrio.Add(JsonConvert.DeserializeObject<ModelBarrio>(response.Mensaje));
                    else if (jsonObject.Type == JTokenType.Array)
                        modelo.listBarrio.AddRange(JsonConvert.DeserializeObject<List<ModelBarrio>>(response.Mensaje));
                }
                else
                {
                    modelo.RespuestaDTO = new RespuestaDTO()
                    {
                        Codigo = response.Codigo,
                        Descripcion = response.Descripcion,
                        Mensaje = response.Mensaje
                    };
                }
            }
            catch (Exception ex)
            {
                modelo.RespuestaDTO = new RespuestaDTO()
                {
                    Codigo = "-1",
                    Descripcion = ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""),
                    Mensaje = ""
                };
            }

            return modelo;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Crea el flujo de consultar Cobertura
        /// </summary>
        /// <param name="parametrosJson"></param>
        /// <returns></returns>
        public modelConsultaCobertura consultarDireccion(string parametrosJson, string listaDireccion)
        {
            String direccionSeleccionada = "";
            JObject jsonObject;
            JToken jsonToken;
            JObject jsonDireccion = JObject.Parse(parametrosJson);
            ModelDireccion _direccion = new ModelDireccion();
            modelo.RespuestaDTO = new RespuestaDTO();
            modelo.listDireccion = new List<ModelDireccion>();
            
            try
            {
                //Consultar dirección si no se han ingresado complementos
                if (string.IsNullOrEmpty(jsonDireccion["complementsSelected"].ToString()))
                {
                    RespuestaDTO response = new FachadaDireccion().consultarDireccion(Servicios.url_Direccion, parametrosJson);

                    if (response.Exito)
                    {
                        jsonObject = JObject.Parse(response.Mensaje);

                        switch (jsonObject["tipoRespuesta"].ToString().ToUpper())
                        {
                            case "OK":
                                #region OK
                                //En caso de que la validación por primera vez sea exitosa, se debe agregar el campo de direccionSeleccionada para la consulta de cobertura
                                if (jsonDireccion["direccionSeleccionada"] != null && string.IsNullOrEmpty(jsonDireccion["direccionSeleccionada"].ToString()))
                                {
                                    direccionSeleccionada = jsonObject["detalleRespuesta"]["detalleDireccion"].ToString();
                                    jsonDireccion.Remove("direccionSeleccionada");
                                    jsonDireccion.Add("direccionSeleccionada", jsonObject["detalleRespuesta"]["detalleDireccion"]);
                                }
                                modelo.RespuestaDTO.Mensaje = "OK";
                                break;
                            #endregion

                            case "DIRECCION":
                                #region DIRECCION
                                modelo.listDireccion = new List<ModelDireccion>();
                                jsonToken = jsonObject["detalleRespuesta"]["detalleDireccion"];

                                if (jsonToken.Type == JTokenType.Object)
                                    modelo.listDireccion.Add(JsonConvert.DeserializeObject<ModelDireccion>(jsonToken.ToString()));
                                else if (jsonToken.Type == JTokenType.Array)
                                {
                                    foreach (var item in jsonToken.Children())
                                    {
                                        if (item["Direccion"] != null)
                                            modelo.listDireccion.Add(new ModelDireccion { Direccion = item["Direccion"].ToString() + " " + item["Placa"].ToString() + " " + item["DesBar"].ToString(), direccionSeleccionada = item.ToString() });
                                    }
                                }

                                modelo.RespuestaDTO.Mensaje = "DIRECCION";
                                break;
                            #endregion

                            case "COMPLEMENTOS":
                                #region COMPLEMENTOS
                                modelo.listComplementoList = new List<ModelComplementoList>();
                                modelo.listTiposPadre = new List<String>();
                                modelo.listComplemento = new List<ModelComplemento>();
                                ModelComplementoList _complementolist = new ModelComplementoList();
                                ModelComplemento objetoModel = new ModelComplemento();
                                List<String> _tipospadres = new List<string>();
                                List<ModelComplemento> listgeneral = new List<ModelComplemento>();

                                jsonToken = JToken.Parse(jsonObject["detalleRespuesta"].ToString());
                                if (jsonToken["LocalComplements"].Type == JTokenType.Object)
                                {
                                    objetoModel = JsonConvert.DeserializeObject<ModelComplemento>(jsonToken["LocalComplements"].ToString());
                                    objetoModel.father = objetoModel.type;
                                    _tipospadres.Add(objetoModel.type);
                                    _tipospadres.Insert(0, "Seleccione un item");
                                    listgeneral.Add(objetoModel);

                                    modelo.listTiposPadre = _tipospadres;
                                    modelo.listComplemento = listgeneral;
                                }
                                else if (jsonToken["LocalComplements"].Type == JTokenType.Array)
                                {
                                    listgeneral = JsonConvert.DeserializeObject<List<ModelComplemento>>(jsonToken["LocalComplements"].ToString());
                                    _tipospadres = listgeneral.GroupBy(tipo => tipo.type).Where(g => g.Count() >= 1 && g.Where(padre => padre.father == "" || padre.father == null).ToList().Count > 0)
                                                                                         .Distinct().Select(g => g.Key).ToList();

                                    foreach (ModelComplemento item in listgeneral)
                                    {
                                        if (string.IsNullOrEmpty(item.father))
                                            item.father = item.type;
                                    }

                                    _tipospadres.Insert(0, "Seleccione un item");
                                    modelo.listTiposPadre = _tipospadres;
                                    modelo.listComplemento = listgeneral;
                                }

                                //Obtener el campo direccionSeleccionada si contiene valores
                                if (jsonToken["detalleDireccion"] != null && !string.IsNullOrEmpty(jsonToken["detalleDireccion"].ToString()))
                                    direccionSeleccionada = jsonToken["detalleDireccion"].ToString();

                                modelo.RespuestaDTO.Mensaje = "COMPLEMENTOS";
                                break;
                                #endregion
                        }
                        
                        if (listaDireccion != null && listaDireccion != "null")
                        {
                            modelo.listDireccion = new List<ModelDireccion>();
                            modelo.listDireccion = JsonConvert.DeserializeObject<List<ModelDireccion>>(listaDireccion);
                        }

                        if (modelo.listDireccion == null || modelo.listDireccion.Count == 0)
                        {
                            modelo.listDireccion = new List<ModelDireccion>();
                            _direccion = JsonConvert.DeserializeObject<ModelDireccion>(parametrosJson);
                            _direccion.direccionSeleccionada = (string.IsNullOrEmpty(_direccion.direccionSeleccionada) ? direccionSeleccionada : "");
                            modelo.listDireccion.Add(_direccion);
                        }
                    }
                    else
                    {
                        modelo.RespuestaDTO = new RespuestaDTO()
                        {
                            Codigo = "-1",
                            Descripcion = response.Descripcion,
                            Mensaje = "ERROR"
                        };
                        return modelo;
                    }
                }
                else
                    modelo.RespuestaDTO.Mensaje = "OK";

                //Validar cobertura con los datos de la dirección seleccionada 
                if (modelo.RespuestaDTO.Mensaje.Equals("OK"))
                    modelo = consultarCobertura(jsonDireccion, JObject.Parse(jsonDireccion["direccionSeleccionada"].ToString()), parametrosJson);
            }
            catch (Exception ex)
            {
                modelo.RespuestaDTO = new RespuestaDTO()
                {
                    Codigo = "-1",
                    Descripcion = ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""),
                    Mensaje = "ERROR"
                };
            }

            return modelo;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Crear dirección en ATIS y consultar cobertura
        /// </summary>
        /// <param name="jsonDireccion">JSON de dirección</param>
        /// <param name="jsonDirSeleccionada">JSON de dirección seleccionada</param>
        /// <returns></returns>
        public modelConsultaCobertura consultarCobertura(JObject _jsonDireccion, JObject _jsonDirSeleccionada, string direccionConsultada)
        {
            RespuestaDTO _respuestaCrear = new RespuestaDTO();
            CrearDir.ColombianPropertyAddress _direccionCrear = new CrearDir.ColombianPropertyAddress();
            CrearDir.ColombianPropertyAddress _direccionCreada = new CrearDir.ColombianPropertyAddress();
            Cobertura.ColombianPropertyAddress _direccionCobertura;
            List<GenericResourceExtendsCobertura> DatosCobertura = new List<GenericResourceExtendsCobertura>();
            JObject _detalleCobertura = new JObject();
            JObject _respuestaJson = new JObject();
            modelo.RespuestaDTO = new RespuestaDTO();
            Cobertura.ResponseValidarCobertura responseCobertura;
            string mensajeResponse = "";
            string errorCode = "";
            string detalle = "";
            try
            {
                List<string> lstDireccion = _jsonDirSeleccionada["DirSplit"].ToString().Split('|').ToList();
                _direccionCrear = mapperDireccionCrear(_jsonDireccion, _jsonDirSeleccionada);
                _direccionCrear = CoberturaBL.AgregarParametrosAdicionalesDireccion(_direccionCrear, lstDireccion);

                //Crear dirección en ATIS
                _respuestaCrear = new FachadaCrearDireccion().CrearDireccionAtis(Servicios.url_CrearDireccion, _direccionCrear);
                _direccionCreada = JsonConvert.DeserializeObject<CrearDir.ColombianPropertyAddress>(_respuestaCrear.Descripcion);
                if (_respuestaCrear.Exito)
                {
                    //Consultar cobertura
                    _direccionCobertura = mapperDireccionCobertura(_direccionCreada, _direccionCrear);
                    if (_direccionCobertura != null)
                    {
                        RespuestaDTO _respuestaCobertura = new FachadaCobertura().consultarCoberturaDos(Servicios.url_Cobertura, _direccionCobertura);

                        if (_respuestaCobertura.Exito)
                        {
                            responseCobertura = JsonConvert.DeserializeObject<Cobertura.ResponseValidarCobertura>(_respuestaCobertura.Descripcion);
                            ResultadoCobertura<Cobertura.GenericResource, Cobertura.ColombianPropertyAddress> resultadoCoberturaDatos =
                            CoberturaBL.InterpretarRespuestaCoberturaDatosOSB(responseCobertura.Resultado, responseCobertura.ColombianPropertyAddress);

                            //Procesa la respuesta de Cobertura
                            ResultadoMensajesCobertura resultadoMensaje = CoberturaBL.ProcesarConsultaCobertura(resultadoCoberturaDatos.ResultadoPrincipal);

                            errorCode = (from d in resultadoCoberturaDatos.ListaCobertura where d.id == "errorCode" select d.description).FirstOrDefault().ToString();

                            if (!string.IsNullOrEmpty(errorCode) && errorCode != "0")
                            {
                                #region Consultar causal

                                RespuestaDTO _respuestaCausal = new FachadaCausal().ConsultarCausal(Servicios.url_Causal);
                                CrearDir.DC_XDictionary _listaCausal = JsonConvert.DeserializeObject<CrearDir.DC_XDictionary>(_respuestaCausal.Mensaje);
                                var descCausal_ = _listaCausal.CollectionXDictionary.Where(lstCau => lstCau._subDictionary.code == errorCode).ToList();
                                String descCausal = "No se encontro el codigo " + errorCode;
                                if (descCausal_!= null && descCausal_.Count > 0 )
                                    descCausal = descCausal_.FirstOrDefault().name.ToString();

                                detalle = string.Format(@"'Mensaje':'No se encontro informacion de cobertura para la ubicacion indicada', 'detalleDireccion': {0},'RespuestaCobertura':'{1}','Estrato':'{2}','Causal':'{3}','codResponse':'{4}'", direccionConsultada, "", "", descCausal, "0");
                                #endregion
                            }
                            else if (responseCobertura.Resultado.Exito && (from d in resultadoCoberturaDatos.ListaCobertura where d.description.Contains("No hay disponibilidad") select d).Count() <= 0
                                && !string.IsNullOrEmpty(resultadoMensaje.NombreLlaveMensaje) && !resultadoMensaje.NombreLlaveMensaje.Contains("Disponibilidad"))
                            {
                                #region Procesar cobertura y estrato                            

                                //Obtener mensaje de cobertura
                                foreach (var item in ((Enumeradores.LlaveCobertura[])Enum.GetValues(typeof(Enumeradores.LlaveCobertura))))
                                {
                                    if (item.ToString() == resultadoMensaje.NombreLlaveMensaje)
                                    {
                                        _respuestaJson.Add(new JProperty("servicioCobertura", resultadoMensaje.NombreLlaveMensaje));
                                        _respuestaJson.Add(new JProperty("descripcionCobertura", (resultadoMensaje.AplicaVelocidad ? string.Format(item.ToStringAttribute(), resultadoMensaje.Velocidad)
                                                           : item.ToStringAttribute())));
                                        break;
                                    }
                                }

                                if (resultadoCoberturaDatos.ListaCobertura != null && resultadoCoberturaDatos.ListaCobertura.Length > 0)
                                {

                                    foreach (Cobertura.GenericResource item in resultadoCoberturaDatos.ListaCobertura)
                                    {
                                        if ((item.id == Enumeradores.PropCoberturaDisponibiidad.distributor.ToString() || item.id == Enumeradores.PropCoberturaDisponibiidad.finalHouse.ToString() ||
                                            item.id == Enumeradores.PropCoberturaDisponibiidad.speedDown.ToString() || item.id == Enumeradores.PropCoberturaDisponibiidad.box.ToString() ||
                                            item.id == Enumeradores.PropCoberturaDisponibiidad.boxPair.ToString() || item.id == Enumeradores.PropCoberturaDisponibiidad.boxType.ToString() ||
                                            item.id == Enumeradores.PropCoberturaDisponibiidad.rangeZoneId.ToString() || item.id == Enumeradores.PropCoberturaDisponibiidad.Disponibilidad.ToString() ||
                                            item.id == Enumeradores.PropCoberturaDisponibiidad.technology.ToString() || item.id == Enumeradores.PropCoberturaDisponibiidad.centralCode.ToString() ||
                                            item.id == Enumeradores.PropCoberturaDisponibiidad.closet.ToString()))
                                        {
                                            if (item.id == Enumeradores.PropCoberturaDisponibiidad.technology.ToString())
                                                item.description = CoberturaBL.ValidarTecnologiaMostrar(resultadoCoberturaDatos.ResultadoPrincipal);


                                            if (item.id == Enumeradores.PropCoberturaDisponibiidad.Disponibilidad.ToString())
                                            {
                                                List<Cobertura.GenericResource> lstDisponibilidad = resultadoCoberturaDatos.ListaCobertura.Where(respuestaCobertura => respuestaCobertura.id ==
                                                                                                    Enumeradores.PropCoberturaDisponibiidad.Disponibilidad.ToString()).ToList();

                                                //Valida si una de las dos disponibilidades(ADSL/VDSL) es verdadera
                                                lstDisponibilidad.ForEach(resulLst =>
                                                {
                                                    if (!string.IsNullOrEmpty(resulLst.description) && resulLst.description.ConvertObjectToBoolean())
                                                        item.description = "true";
                                                });
                                            }
                                            DatosCobertura.Add(new GenericResourceExtendsCobertura() { id = item.id, description = item.description });
                                        }
                                    }
                                }

                                //Consultar callejero para determinar el estrato
                                Callejero.Interfaz1RequestBody _requestCallejero = mapperCallejero(_direccionCobertura);
                                RespuestaDTO _respuestaCallejero = new FachadaCallejero().ConsultarCallejero(Servicios.url_Callejero, _requestCallejero);

                                //Llenar objeto JSON de respuesta
                                _respuestaJson.Add(new JProperty("detalleCobertura", (DatosCobertura != null && DatosCobertura.Count > 0 ? JToken.FromObject(DatosCobertura) : "")));
                                detalle = string.Format(@"'Mensaje':'Para la siguiente direccion se dispone de la siguiente tecnologia','detalleDireccion':{0},'RespuestaCobertura':'{1}','Estrato':'{2}','Causal':'{3}','codResponse':'{4}'", direccionConsultada, _respuestaJson.ToString(), (_respuestaCallejero.Exito ? _respuestaCallejero.Mensaje : ""), "", "0");

                                #endregion
                            }
                            else if ((from d in resultadoCoberturaDatos.ListaCobertura where d.description.Contains("No hay disponibilidad") select d).Count() > 0 ||
                                       !string.IsNullOrEmpty(resultadoMensaje.NombreLlaveMensaje) && resultadoMensaje.NombreLlaveMensaje.Contains("Disponibilidad"))
                            {
                                //No disponibilidad
                                detalle = string.Format(@"'Mensaje':'No hay disponibilidad para la direccion indicada','detalleDireccion':{0},'RespuestaCobertura':'{1}', 'Estrato':'{2}','Causal':'{3}','codResponse':'{4}'", direccionConsultada, "", "", "", "0");
                            }
                            else if (!responseCobertura.Resultado.Exito)
                            {
                                //No cobertura
                                detalle = string.Format(@"'Mensaje':'No se encontro informacion de cobertura para la ubicacion indicada','detalleDireccion': {0},  'RespuestaCobertura':'{1}','Estrato':'{2}','Causal':'{3}','codResponse':'{4}'", direccionConsultada, "", "", "", "0");
                            }

                            //mensajeResponse = "COBERTURA";

                            modelo.RespuestaDTO.Exito = !string.IsNullOrEmpty(detalle);
                            modelo.RespuestaDTO.Mensaje = !string.IsNullOrEmpty(detalle) ? "COBERTURA" : "";
                            modelo.RespuestaDTO.Descripcion = detalle;
                        }
                        else
                        {
                            modelo.RespuestaDTO = new RespuestaDTO
                            {
                                Codigo = "-1",
                                Descripcion = _respuestaCobertura.Mensaje,
                                Mensaje = "ERROR"
                            };
                        }
                    }
                    else
                        modelo.RespuestaDTO.Mensaje = "ERROR";
                }
            }
            catch (Exception ex)
            {
                modelo.RespuestaDTO = new RespuestaDTO()
                {
                    Codigo = "-1",
                    Descripcion = ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""),
                    Mensaje = "ERROR",
                    Exito = false
                };
            }
            return modelo;
        }

        #region Mapper objetos de fachadas

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Mapear dirección para crear en ATIS
        /// </summary>
        /// <param name="_jsonDireccion">Objeto con parametros de dirección</param>
        /// <param name="_jsonDirSeleccionada">Objeto de dirección seleccionada</param>
        /// <returns>Dirección para creación en ATIS</returns>
        public CrearDir.ColombianPropertyAddress mapperDireccionCrear(JObject _jsonDireccion, JObject _jsonDirSeleccionada)
        {
            CrearDir.ColombianPropertyAddress _direccion = new CrearDir.ColombianPropertyAddress();
            string complementos = "";
            _direccion.stateOrProvince = new CrearDir.State();
            _direccion.locality = new CrearDir.GeographicArea();
            _direccion.GeographicZone = new CrearDir.GeographicArea();
            _direccion.city = new CrearDir.GeographicArea();
            _direccion.subLocality = new CrearDir.GeographicArea();
            _direccion.typeLegacy = new CrearDir.Dictionary();
            _direccion.addressType = new CrearDir.xDictionary();
            _direccion.stratum = new CrearDir.xDictionary();
            _direccion.SuggestedDisplayOptions = new CrearDir.AddressDisplayOptions();
            _direccion.RefStatus = new CrearDir.xDictionary();
            _direccion.RefStatus._equality = new CrearDir.xEquality[] { };
            _direccion.complements = new string[7];

            //Cargar datos de dirección            
            _direccion.stateOrProvince.name = _jsonDireccion["Departamento"].ToString();
            _direccion.stateOrProvince.code = _jsonDireccion["codAtisDepartamento"].ToString();
            _direccion.city.name = _jsonDireccion["Municipio"].ToString();
            _direccion.city.code = _jsonDireccion["codAtisMunicipio"].ToString();
            _direccion.city.DANECode = _jsonDireccion["codDaneMunicipio"].ToString();
            _direccion.locality.name = _jsonDireccion["nomLocalidad"].ToString();
            _direccion.locality.code = _jsonDireccion["codAtisLocalidad"].ToString();
            _direccion.locality.DANECode = _jsonDireccion["codDaneLocalidad"].ToString();
            _direccion.fullAddress = _jsonDireccion["Direccion"].ToString();
            _direccion.subLocality.name = _jsonDireccion["nomBarrio"].ToString();
            _direccion.subLocality.code = _jsonDireccion["codAtisBarrio"].ToString();
            _direccion.SplitAddress = _jsonDirSeleccionada["DirSplit"].ToString();
            _direccion.typeLegacy.code = "S";
            _direccion.typeLegacy.name = "Callejero";
            _direccion.IsADSL = true;
            _direccion.IsVDSL = true;
            _direccion.RefStatus.code = "B";
            _direccion.RefStatus.name = "";
            _direccion.streetNrLast = _jsonDirSeleccionada["Placa"].ToString();
            _direccion.complements[1] = (_jsonDirSeleccionada["Complemento2"] != null && _jsonDirSeleccionada["Complemento2"].ToString() != ""
                                        && _jsonDirSeleccionada["Complemento2"].ToString().ToUpper().Contains("LAT") ? _jsonDirSeleccionada["Complemento2"].ToString() : "");

            //Determinar el código de la zona geografica
            foreach (var item in ((Enumeradores.ZonasGeograficas[])Enum.GetValues(typeof(Enumeradores.ZonasGeograficas))))
            {
                if (item.ToString() == _jsonDireccion["Departamento"].ToString().Replace(" ", "").ToUpper())
                {
                    _direccion.GeographicZone.code = item.ToStringAttribute();
                    break;
                }
            }
            //Cargar los complementos
            if (_jsonDireccion["complementsSelected"] != null && _jsonDireccion["complementsSelected"].HasValues)
            {
                foreach (JToken item in _jsonDireccion["complementsSelected"].Children())
                    complementos += ((string.IsNullOrEmpty(complementos) ? "" : "*")) + item["type"].ToString() + ":" + item["value"].ToString();
                    
                _direccion.complements[0] = (!string.IsNullOrEmpty(complementos) ? complementos : "");
            }
            return _direccion;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Mapear dirección del namespace ServicioUbicacion a dirección de cobertura
        /// </summary>
        /// <param name="_direccion">Direccion namespace de ServicioUbicacion</param>
        /// <returns>Dirección para consultar cobertura</returns>
        public Cobertura.ColombianPropertyAddress mapperDireccionCobertura(CrearDir.ColombianPropertyAddress _direccionCreada, CrearDir.ColombianPropertyAddress _direccionCopia)
        {
            Cobertura.ColombianPropertyAddress _direccion = new Cobertura.ColombianPropertyAddress();

            _direccion.stateOrProvince = new Cobertura.State();
            _direccion.locality = new Cobertura.GeographicArea();
            _direccion.GeographicZone = new Cobertura.GeographicArea();
            _direccion.city = new Cobertura.GeographicArea();
            _direccion.subLocality = new Cobertura.GeographicArea();
            _direccion.complements = new string[7];
            _direccion.typeLegacy = new Cobertura.Dictionary();
            _direccion.addressType = new Cobertura.xDictionary();
            _direccion.stratum = new Cobertura.xDictionary();
            _direccion.SuggestedDisplayOptions = new Cobertura.AddressDisplayOptions();
            _direccion.RefStatus = new Cobertura.xDictionary();
            _direccion.RefStatus._equality = new Cobertura.xEquality[] { };
            _direccion.subAddress = new Cobertura.UrbanPropertySubAddress();
            _direccion.subAddress.levelType = new Cobertura.xDictionary();
            _direccion.streetType = new Cobertura.xDictionary();
            _direccion.streetType._subDictionary = new Cobertura.xDictionary();
            
            //Cargar datos de dirección
            _direccion.id = (!string.IsNullOrEmpty(_direccionCreada.id) ? _direccionCreada.id.Trim() : "");
            if (_direccionCopia.stateOrProvince != null)
            {
                _direccion.stateOrProvince.name = _direccionCopia.stateOrProvince.name;
                _direccion.stateOrProvince.code = _direccionCopia.stateOrProvince.code;
            }
            if (_direccionCopia.city != null)
            {
                _direccion.city.name = _direccionCopia.city.name;
                _direccion.city.code = _direccionCopia.city.code;
                _direccion.city.DANECode = _direccionCopia.city.DANECode;
            }
            if (_direccionCopia.locality != null)
            {
                _direccion.locality.name = _direccionCopia.locality.name;
                _direccion.locality.code = _direccionCopia.locality.code;
                _direccion.locality.DANECode = _direccionCopia.locality.DANECode;
            }
            _direccion.GeographicZone.code = (_direccionCopia.GeographicZone != null ? _direccionCopia.GeographicZone.code : "");
            _direccion.fullAddress = (_direccionCopia.fullAddress != null ? _direccionCopia.fullAddress : "");
            if (_direccionCopia.subLocality != null)
            {
                _direccion.subLocality.name = _direccionCopia.subLocality.name;
                _direccion.subLocality.code = _direccionCopia.subLocality.code;
            }

            //Parametros adicionales de dirección
            _direccion.streetSuffix = _direccionCopia.streetSuffix;
            _direccion.streetName = _direccionCopia.streetName;
            _direccion.streetNrFirst = _direccionCopia.streetNrFirst;
            _direccion.streetNrFirstSuffix = _direccionCopia.streetNrFirstSuffix;
            _direccion.streetNrLast = _direccionCopia.streetNrLast;
            if (_direccionCopia.streetType != null)
            {
                _direccion.streetType.name = (!string.IsNullOrEmpty(_direccionCopia.streetType.name) ? _direccionCopia.streetType.name : "");
                _direccion.streetType._subDictionary.code = (_direccionCopia.streetType._subDictionary != null ? _direccionCopia.streetType._subDictionary.code : "");
            }
            _direccion.streetType.code = ((int)Enumeradores.TipoConsultaCobertura.LineaBasica).ToString();

            if (_direccionCopia.subAddress != null)
            {
                _direccion.subAddress.levelNr = _direccionCopia.subAddress.levelNr;
                if (_direccionCopia.subAddress.levelType._equality != null && _direccionCopia.subAddress.levelType._equality.Length > 0)
                {
                    _direccion.subAddress.levelType._equality = new Cobertura.xEquality[_direccionCopia.subAddress.levelType._equality.Count()];
                    for (int i = 0; i < _direccionCopia.subAddress.levelType._equality.Length; i++)
                    {
                        _direccion.subAddress.levelType._equality[i] = new Cobertura.xEquality();
                        _direccion.subAddress.levelType._equality[i].code = _direccionCopia.subAddress.levelType._equality[i].code;
                    }
                }
            }
            _direccion.SplitAddress = (_direccionCreada.SplitAddress != null ? _direccionCreada.SplitAddress : "");
            _direccion.complements[0] = (_direccionCreada.complements != null && _direccionCreada.complements.Count() > 0 ? _direccionCreada.complements[0] : "");
            _direccion.complements[1] = (_direccionCreada.complements != null && _direccionCreada.complements.Count() > 0 ? _direccionCreada.complements[1] : "");
            _direccion.typeLegacy.code = "S";
            _direccion.typeLegacy.name = "Callejero";
            _direccion.IsADSL = true;
            _direccion.IsVDSL = true;
            _direccion.RefStatus.code = _direccionCreada.RefStatus.code;
            _direccion.RefStatus.name = _direccionCreada.RefStatus.name;
            if (_direccionCreada.RefStatus._equality != null && _direccionCreada.RefStatus._equality.Length > 0)
            {
                _direccion.RefStatus._equality = new Cobertura.xEquality[_direccionCreada.RefStatus._equality.Count()];
                for (int i = 0; i < _direccionCreada.RefStatus._equality.Length; i++)
                {
                    _direccion.RefStatus._equality[i] = new Cobertura.xEquality();
                    _direccion.RefStatus._equality[i].code = _direccionCreada.RefStatus._equality[i].code;
                }
            }

            return _direccion;
        }

        /// <summary>
        /// Autor: Juliana Polo
        /// Fecha: 11-Dic-2018
        /// Mapear dirección de cobertura a dirección para callejero
        /// </summary>
        /// <param name="_direccion">Direccion namespace de cobertura</param>
        /// <returns>Dirección para consultar callejero</returns>
        public Callejero.Interfaz1RequestBody mapperCallejero(Cobertura.ColombianPropertyAddress _direccion)
        {
            Callejero.Interfaz1RequestBody _requestCallejero = new Callejero.Interfaz1RequestBody();
            _requestCallejero.strDep = _direccion.locality.DANECode.Length >= 2 ? _direccion.locality.DANECode.Substring(0, 2) : string.Empty;
            _requestCallejero.strMun = _direccion.locality.DANECode.Length >= 5 ? _direccion.locality.DANECode.Trim().Substring(2, 3) : string.Empty;
            _requestCallejero.strLoc = _direccion.locality.DANECode.Length >= 8 ? _direccion.locality.DANECode.Trim().Substring(5, 3) : string.Empty;
            _requestCallejero.strBarr = _direccion.subLocality.code + "|" + _direccion.subLocality.name;
            _requestCallejero.strDir = _direccion.fullAddress;
            _requestCallejero.strOpcionCompleto = "1";
            return _requestCallejero;
        }
        #endregion
    }
}
