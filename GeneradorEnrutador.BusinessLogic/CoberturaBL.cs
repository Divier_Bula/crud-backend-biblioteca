﻿using System;
using System.Collections.Generic;
using System.Linq;
using ModelCobertura;
using static Common.Utilities.Enumeradores;
using Cobertura = serviceProxy.ServicioOfertaComercial;
using CrearDir = serviceProxy.ServicioUbicacion;

namespace GeneradorEnrutador.BusinessLogic
{
    public static class CoberturaBL 
    {   
        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// Interpreta la respuesta del servicio de Consulta de Cobertura</summary>
        /// <param name="resultadoServicio">Resultado del servicio de CCA que Consulta Cobertura</param>
        /// <param name="resultadoLegado">Resultado del servicio de OSB</param>
        /// <param name="incluirGenericResources">Indica si se debe incluir la lista de GenericResources</param>
        /// <returns>Entidad de tipo ResultadoCobertura con la interpratación de la respuesta de Cobertura</returns>
        private static ResultadoCobertura RestriccionesCoberturaOSB(Cobertura.Resultado resultadoServicio, Cobertura.ColombianPropertyAddress resultadoLegado)
        {
            ResultadoCobertura respuesta = new ResultadoCobertura() { ExitoServicio = true };

            //Valdiaciones error en servicio de Cobertura
            if (resultadoServicio == null || !resultadoServicio.Exito || (!string.IsNullOrEmpty(resultadoServicio.Mensaje) && resultadoServicio.Mensaje.Contains("Hay campos obligatorios sin informar para cobertura")))
            {
                respuesta.ExitoServicio = false;
                respuesta.MensajeServicio = resultadoServicio != null && !string.IsNullOrEmpty(resultadoServicio.Mensaje) ? resultadoServicio.Mensaje : "No hay respuesta de Cobertura";
                return respuesta;
            }

            //Validaciones consulta pedido impeditivo
            if (resultadoLegado != null && (resultadoLegado._genericResource != null && resultadoLegado._genericResource.Length > 0))
            {
                if (resultadoLegado.locality != null)
                    respuesta.CodigoDANELocalidad = resultadoLegado.locality.DANECode;

                //Asigna las prodpiedades de Restricción de Cobertura por Localidad, invirtiendo los Booleanos debido a que en la respuesta del servicio invierten los resultados
                Cobertura.GenericResource restriccionLB = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.RCL_LineaDB.ToString()).FirstOrDefault();
                if (restriccionLB != null)
                    respuesta.RestriccionVoz = ConvertObjectToBoolean(restriccionLB.description);

                Cobertura.GenericResource restriccionBACE = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.RCL_BandaAnchaCEDB.ToString()).FirstOrDefault();
                if (restriccionBACE != null)
                    respuesta.RestriccionBACE = ConvertObjectToBoolean(restriccionBACE.description);

                Cobertura.GenericResource restriccionBACN = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.RCL_BandaAnchaCNDB.ToString()).FirstOrDefault();
                if (restriccionBACN != null)
                    respuesta.RestriccionBACN = ConvertObjectToBoolean(restriccionBACN.description);

                Cobertura.GenericResource restriccionVelocidad = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.RCL_RestriccionVelocidadBADB.ToString()).FirstOrDefault();
                if (restriccionVelocidad != null)
                    respuesta.RestriccionBAVelocidad = ConvertObjectToBoolean(restriccionVelocidad.description);

                Cobertura.GenericResource consultaRestriccion = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.ConsultaRestriccionCobertura.ToString()).FirstOrDefault();
                if (consultaRestriccion != null)
                    respuesta.ConsultaRestriccionCobertura = ConvertObjectToBoolean(consultaRestriccion.description);

                //Valida el tipo de Localidad (Naked o Tradicional) de la tabla de restricción
                Cobertura.GenericResource [] tipoLocalidad = resultadoLegado._genericResource.Where(tipo => tipo.id == PropCoberturaDisponibiidad.TipoLocalidadCobertura.ToString()).ToArray();
                if (tipoLocalidad != null && tipoLocalidad.Length > 0)
                    respuesta.TipoLocalidadRestriccion = tipoLocalidad.LastOrDefault().description;

                Cobertura.GenericResource [] velocidad = resultadoLegado._genericResource.Where(tipo => tipo.id == PropCoberturaDisponibiidad.speedDown.ToString()).ToArray();
                if (velocidad != null && velocidad.Length > 0)
                {
                    int velocidadDes = 0;
                    int.TryParse(velocidad.LastOrDefault().description, out velocidadDes);
                    respuesta.VelocidadDescarga = velocidadDes;
                }
            }

            //Valida si la Consulta de Cobertura respondió datos
            if (resultadoServicio != null && (resultadoServicio.CodigosRespuesta != null && resultadoServicio.CodigosRespuesta.Length > 0))
            {
                //Valida si Cobertura está apagado
                if (!string.IsNullOrEmpty(resultadoServicio.CodigosRespuesta.FirstOrDefault().Descripcion) &&
                    resultadoServicio.CodigosRespuesta.FirstOrDefault().Descripcion.Contains("La consulta del servicio de Cobertura se encuentra apagada."))
                {
                    respuesta.MensajeServicio = "El servicio de Cobertura se encuentra Apagado.";
                    respuesta.CoberturaApagado = true;
                }
                else
                {
                    //Valida que haya un respuesta positiva del servicio de Cobertura
                    if (resultadoServicio.CodigosRespuesta.ElementAt(0).ResultadoExitoLegado && (resultadoLegado._genericResource != null && resultadoLegado._genericResource.Length > 0))
                    {
                        //Validaciones de tipo de producto
                        Cobertura.GenericResource serviciosCobertura = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.ServicioCobertura.ToString()).FirstOrDefault();

                        //Valida que haya una respuesta del tipo de servicio que se puede ofrecer
                        if (serviciosCobertura != null)
                        {
                            Cobertura.GenericResource coberturaServicios = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.Cobertura.ToString()).FirstOrDefault();

                            if (coberturaServicios != null)
                            {
                                respuesta.Cobertura = ConvertObjectToBoolean(coberturaServicios.description);
                            }

                            //Productos Convencionales
                            if (serviciosCobertura.description == "R")
                            {
                                Cobertura.GenericResource dispoLB = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadLBTradicional.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoBAADSL = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadBAConvencionalADSL.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoBAVDSL = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadBAConvencionalVDSL.ToString()).FirstOrDefault();

                                if (dispoLB != null)
                                {
                                    respuesta.DisponibilidadLBTradicional = ConvertObjectToBoolean(dispoLB.description);
                                }

                                if (dispoBAADSL != null)
                                {
                                    respuesta.DisponibilidadBAConvencionalADSL = ConvertObjectToBoolean(dispoBAADSL.description);
                                }

                                if (dispoBAVDSL != null)
                                {
                                    respuesta.DisponibilidadBAConvencionalVDSL = ConvertObjectToBoolean(dispoBAVDSL.description);
                                }
                            }
                            //Servicios Naked
                            else if (serviciosCobertura.description == "N")
                            {
                                Cobertura.GenericResource dispoBAADSL = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadNakedADSL.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoBAVDSL = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadNakedVDSL.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoVozIP = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadVozIP.ToString()).FirstOrDefault();

                                if (dispoBAADSL != null)
                                {
                                    respuesta.DisponibilidadBANakedADSL = ConvertObjectToBoolean(dispoBAADSL.description);
                                }

                                if (dispoBAVDSL != null)
                                {
                                    respuesta.DisponibilidadBANakedVDSL = ConvertObjectToBoolean(dispoBAVDSL.description);
                                }

                                if (dispoVozIP != null)
                                {
                                    respuesta.DisponibilidadVozIP = ConvertObjectToBoolean(dispoVozIP.description);
                                }
                            }
                            //Servicios Fibra
                            else if (serviciosCobertura.description == "F")
                            {
                                Cobertura.GenericResource dispoBAFibra = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadBAFibra.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoVozIP = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadVozIP.ToString()).FirstOrDefault();

                                if (dispoBAFibra != null)
                                {
                                    respuesta.DisponibilidadBAFibra = ConvertObjectToBoolean(dispoBAFibra.description);
                                }

                                if (dispoVozIP != null)
                                {
                                    respuesta.DisponibilidadVozIP = ConvertObjectToBoolean(dispoVozIP.description);
                                }
                            }
                            //Servicios Fibra-Naked
                            else if (serviciosCobertura.description == "FN")
                            {
                                Cobertura.GenericResource dispoBAADSL = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadNakedADSL.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoBAVDSL = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadNakedVDSL.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoBAFibra = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadBAFibra.ToString()).FirstOrDefault();
                                Cobertura.GenericResource dispoVozIP = resultadoLegado._genericResource.Where(x => x.id == PropCoberturaDisponibiidad.DisponibilidadVozIP.ToString()).FirstOrDefault();

                                if (dispoBAADSL != null)
                                {
                                    respuesta.DisponibilidadBANakedADSL = ConvertObjectToBoolean(dispoBAADSL.description);
                                }

                                if (dispoBAVDSL != null)
                                {
                                    respuesta.DisponibilidadBANakedVDSL = ConvertObjectToBoolean(dispoBAVDSL.description);
                                }

                                if (dispoBAFibra != null)
                                {
                                    respuesta.DisponibilidadBAFibra = ConvertObjectToBoolean(dispoBAFibra.description);
                                }

                                if (dispoVozIP != null)
                                {
                                    respuesta.DisponibilidadVozIP = ConvertObjectToBoolean(dispoVozIP.description);
                                }
                            }
                        }
                        else
                        {
                            respuesta.ExitoServicio = false;
                            respuesta.MensajeServicio = "No hay respuesta de Cobertura";
                        }
                    }
                    else
                    {
                        respuesta.ExitoServicio = false;
                        respuesta.MensajeServicio = !string.IsNullOrEmpty(resultadoServicio.CodigosRespuesta.ElementAt(0).Descripcion)
                            ? resultadoServicio.CodigosRespuesta.ElementAt(0).Descripcion : "La consulta de cobertura y disponibilidad presenta fallas";
                    }
                }
            }
            else
            {
                respuesta.ExitoServicio = false;
                respuesta.MensajeServicio = "No hay respuesta de Cobertura";
            }

            return respuesta;
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// Interpreta la respuesta del servicio de Consulta de Cobertura y la validación de Restricción de Cobertura por Localidad</summary>
        /// <param name="resultadoServicio">Resultado del servicio de CCA que Consulta Cobertura</param>
        /// <param name="resultadoLegado">Resultado del servicio de BROKER</param>
        /// <param name="esClienteNuevo">Indica si es la consulta se ejecuta para Cliente Nuevo</param>
        /// <param name="esClienteExistente">Indica si es la consulta se ejecuta para Cliente Existente</param>
        /// <param name="incluirGenericResources">Indica si se debe incluir la lista de GenericResources</param>
        /// <returns>Entidad de tipo ResultadoCobertura con la interpratación de la respuesta de Cobertura y la validación de Restricción de Cobertura por Localidad</returns>
        public static ResultadoCobertura InterpretarRespuestaCoberturaOSB(Cobertura.Resultado resultadoServicio, Cobertura.ColombianPropertyAddress resultadoLegado)
        {
            ResultadoCobertura respuesta = RestriccionesCoberturaOSB(resultadoServicio, resultadoLegado);
            if (respuesta.ExitoServicio.Equals(false))
                return respuesta;


            //Identificar el tipo de Localidad de acuerdo a la respuesta del servicio de Cobertura
            if (respuesta.DisponibilidadLBTradicional && !respuesta.CoberturaApagado && respuesta.ExitoServicio)
            {
                respuesta.EsLocalidadTradicional = true;
            }

            if (respuesta.DisponibilidadInternetFijo && !respuesta.CoberturaApagado && respuesta.ExitoServicio && (respuesta.DisponibilidadBANakedADSL || respuesta.DisponibilidadBANakedVDSL))
            {
                respuesta.EsLocalidadNaked = true;
            }

            if (!respuesta.DisponibilidadLBTradicional && !respuesta.DisponibilidadInternetFijo)
            {
                respuesta.EsLocalidadTradicional = respuesta.TipoLocalidadRestriccion == TipoLocalidadCobertura.Tradicional.ToString();
                respuesta.EsLocalidadNaked = respuesta.TipoLocalidadRestriccion == TipoLocalidadCobertura.Naked.ToString();
            }

            //Validaciones de Restricción de Cobertura por Localidad (Naked)
            if ((respuesta.EsLocalidadNaked && respuesta.ConsultaRestriccionCobertura && !respuesta.DisponibilidadLBTradicional && !respuesta.DisponibilidadInternetFijo) ||
                (respuesta.EsLocalidadTradicional && respuesta.ConsultaRestriccionCobertura && !respuesta.DisponibilidadLBTradicional && !respuesta.DisponibilidadInternetFijo))
            {
                //Validaciones de Restricción de Cobertura por Localidad (Tradicional)
                respuesta.ExitoServicio = true;
            }
            else
            {
                //2016-08-02 GRU: Se realiza modificacion para que se retorne la propiedad en False para que no se presenten mensaje de Restriccion de Cobertura
                respuesta.ConsultaRestriccionCobertura = false;
            }

            return respuesta;
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// Interpretacion de la respuesta de cobertura datos OSB </summary>
        /// <param name="resultadoServicio">Resultado servicio</param>
        /// <param name="resultadoLegado">Resultado legado</param>
        /// <param name="esClienteNuevo">cliente nuevo</param>
        /// <param name="esClienteExistente">cliente existente</param>
        /// <returns></returns>
        public static ResultadoCobertura<Cobertura.GenericResource, Cobertura.ColombianPropertyAddress> InterpretarRespuestaCoberturaDatosOSB(Cobertura.Resultado resultadoServicio, Cobertura.ColombianPropertyAddress resultadoLegado)
        {
            return new ResultadoCobertura<Cobertura.GenericResource, Cobertura.ColombianPropertyAddress>()
            {
                ResultadoPrincipal = InterpretarRespuestaCoberturaOSB(resultadoServicio, resultadoLegado),
                DireccionCobertura = resultadoLegado,
                ListaCobertura = resultadoLegado != null ? resultadoLegado._genericResource : null
            };
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// Procesa las convinaciones posibles que se pueden dar segun la respuesta del servicio OSB</summary>
        /// <param name="resultadoCobertura">resultado del servicio de cobertura del OSB </param>
        /// <returns>El mensaje que se muestra al agente</returns>
        public static ResultadoMensajesCobertura ProcesarConsultaCobertura(ResultadoCobertura resultadoCobertura)
        {
            ResultadoMensajesCobertura respuestaMensaje = new ResultadoMensajesCobertura()
            {
                MensajePorServicio = true,
                CoberturaValida = true
            };

            //Disponibilidad LB
            if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, true, false, false, false, false, false, false, false))
                respuestaMensaje.NombreLlaveMensaje = "ADSL_LB";

            //Disponibilidad de LB+BA(ADSL)
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, true, false, true, false, false, false, false, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_LB_BA_VelMayorCero";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_LB_BA";
            }

            //Disponibilidad de LB+BA(VDSL)
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, true, false, true, true, false, false, false, false)
                || CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, true, false, false, true, false, false, false, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_LB_BA_VelMayorCero";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_LB_BA";
            }

            //Disponibilidad de Naked ADSL
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, false, false, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_VelMayorCero";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_VelMenorCero";
            }

            //Disponibilidad de Naked ADSL+VozIP
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, false, false, true))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_VozIP_VelMayorCero";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_VozIP_VelMenorCero";
            }

            //Disponibilidad de Naked VDSL
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, true, false, false)
                || CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, false, true, false, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_VelMayorCero";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_VelMenorCero";
            }

            //Disponibilidad de Naked VDSL+VozIP
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, true, false, true)
                || CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, false, true, false, true))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_VozIP_VelMayorCero";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_VozIP_VelMenorCero";
            }
            //Disponibilidad de Fibra
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, false, false, true, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "Fibra_Velocidad";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "Fibra";
            }

            //Disponibilidad de Fibra+VozIP
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, false, false, true, true))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "Fibra_VozIP_Velocidad";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "Fibra_VozIP";
            }


            //Disponibilidad de Fibra + Naked ADSL
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, false, true, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_Fibra_Velocidad";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_Fibra";
            }

            //Disponibilidad de Fibra + Naked ADSL + VozIP
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, false, true, true))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_Fibra_VozIP_Velocidad";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "ADSL_Naked_Fibra_VozIP";
            }

            //Disponibilidad de Fibra + Naked VDSL
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, true, true, false)
                || CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, false, true, true, false))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_Fibra_Velocidad";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_Fibra";
            }
            //Disponibilidad de Fibra + Naked VDSL + VozIP
            else if (CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, true, true, true, true)
                || CombinatoriaDisponibilidadCoberturaOSB(resultadoCobertura, false, true, false, false, false, true, true, true))
            {
                if (resultadoCobertura.VelocidadDescarga > 0)
                {
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_Fibra_VozIP_Velocidad";
                    respuestaMensaje.Velocidad = resultadoCobertura.VelocidadDescarga;
                    respuestaMensaje.AplicaVelocidad = true;
                }
                else
                    respuestaMensaje.NombreLlaveMensaje = "VDSL_Naked_Fibra_VozIP";
            }
            else
            {
                if (resultadoCobertura.DisponibilidadLBTradicional && resultadoCobertura.DisponibilidadInternetFijo)
                    respuestaMensaje.NombreLlaveMensaje = "DisponibilidadDireccionIntraducibleNoGeo";
                else
                    respuestaMensaje.NombreLlaveMensaje = "NoDisponibilidad";
            }

            respuestaMensaje.VisibilidadCausalTecnica = !resultadoCobertura.DisponibilidadLBTradicional && !resultadoCobertura.DisponibilidadInternetFijo ? true : false;

            return respuestaMensaje;
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// comparar el resultado de cobertura con la combinatoria de mensaje solicitado</summary>
        /// <param name="resultadoCobertura">Resultado del servicio de Cobertura y Disponibilidad</param>
        /// <param name="disponibilidadLB">Indica si hay disponibilidad de LB Tradicional</param>
        /// <param name="disponibilidadInternetFijo">Indica si hay disponibilidad de Internet Naked</param>
        /// <param name="disponibilidadBAConvencionalADSL">Indica si hay disponibilidad de BA ADSL Convencional</param>
        /// <param name="disponibilidadBAConvencionalVDSL">Indica si hay disponibilidad de BA VDSL Convencional</param>
        /// <param name="disponibilidadBANakedADSL">Indica si hay disponibilidad de BA ADSL Naked</param>
        /// <param name="disponibilidadBANakedVDSL">Indica si hay disponibilidad de BA VDSL Naked</param>
        /// <param name="disponibilidadBAFibra">Indica si hay disponibilidad de BA Fibra</param>
        /// <param name="disponibilidadVozIP">Indica si hay disponibilidad de Voz IP</param>
        /// <returns>retorna si cumple o no con esas condiciones</returns>
        private static bool CombinatoriaDisponibilidadCoberturaOSB(ResultadoCobertura resultadoCobertura, bool disponibilidadLB, bool disponibilidadInternetFijo,
            bool disponibilidadBAConvencionalADSL, bool disponibilidadBAConvencionalVDSL, bool disponibilidadBANakedADSL, bool disponibilidadBANakedVDSL,
            bool disponibilidadBAFibra, bool disponibilidadVozIP)
        {
            return resultadoCobertura.DisponibilidadLBTradicional == disponibilidadLB
                && resultadoCobertura.DisponibilidadInternetFijo == disponibilidadInternetFijo
                && resultadoCobertura.DisponibilidadBAConvencionalADSL == disponibilidadBAConvencionalADSL
                && resultadoCobertura.DisponibilidadBAConvencionalVDSL == disponibilidadBAConvencionalVDSL
                && resultadoCobertura.DisponibilidadBANakedADSL == disponibilidadBANakedADSL
                && resultadoCobertura.DisponibilidadBANakedVDSL == disponibilidadBANakedVDSL
                && resultadoCobertura.DisponibilidadBAFibra == disponibilidadBAFibra
                && resultadoCobertura.DisponibilidadVozIP == disponibilidadVozIP;
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// Agrega los párametros de cobertura a la entidad ColombianPropertyAddress</summary>
        /// <param name="listaDireccion">Lista de tipo string con la dirección dividida</param>
        /// <returns>Entidad de tipo ColombianPropertyAddress con los respectivos datos</returns>
        public static CrearDir.ColombianPropertyAddress AgregarParametrosAdicionalesDireccion(CrearDir.ColombianPropertyAddress direccion, List<string> listaDireccion)
        {
            if (listaDireccion != null && listaDireccion.Count > 0)
            {
                if (listaDireccion.Any())
                {
                    direccion.streetSuffix = listaDireccion.ElementAt(0).Trim();
                    direccion.streetName = listaDireccion.ElementAt(1).Trim();
                    direccion.streetNrFirst = listaDireccion.ElementAt(2).Trim();
                    direccion.streetNrFirstSuffix = listaDireccion.ElementAt(3).Trim();
                    direccion.streetType = new CrearDir.xDictionary();
                    direccion.streetType.name = listaDireccion.ElementAt(4).Trim() ;
                    direccion.subAddress = new CrearDir.UrbanPropertySubAddress()
                    {
                        levelType = new CrearDir.xDictionary()
                        {
                            _equality = new CrearDir.xEquality[]
                            {
                                new CrearDir.xEquality() {  code = listaDireccion.ElementAt(5).Trim() },
                                new CrearDir.xEquality() {  code = listaDireccion.ElementAt(7).Trim() },
                                new CrearDir.xEquality() {  code = listaDireccion.ElementAt(8).Trim() },
                                new CrearDir.xEquality() {  code = listaDireccion.ElementAt(9).Trim() }
                            }
                        },
                        levelNr = listaDireccion.ElementAt(6).Trim()
                    };
                    
                    direccion.streetType._subDictionary = new CrearDir.xDictionary();
                    direccion.streetType._subDictionary.code = listaDireccion.ElementAt(1).Trim();
                }
            }

            return direccion;
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 14-Dic-2018
        /// Determina la tecnologia a mostrar el resultado de cobertura</summary>
        /// <param name="resultadoCobertura">Resultado de la validación de cobertura</param>
        /// <returns>Entidad string con el indicador de tecnologia</returns>
        public static string ValidarTecnologiaMostrar(ResultadoCobertura resultadoCobertura)
        {
            string valorTecnologia = string.Empty;

            if (resultadoCobertura.DisponibilidadLBTradicional)
            {
                if (resultadoCobertura.DisponibilidadBAConvencionalVDSL)
                {
                    valorTecnologia = "VDSL";
                }
                else if (resultadoCobertura.DisponibilidadBAConvencionalADSL)
                {
                    valorTecnologia = "ADSL";
                }
            }
            else if (resultadoCobertura.DisponibilidadInternetFijo)
            {
                if (resultadoCobertura.DisponibilidadBANakedADSL && !resultadoCobertura.DisponibilidadBAFibra)
                {
                    if (resultadoCobertura.DisponibilidadBANakedVDSL)
                    {
                        valorTecnologia = "VDSL";
                    }
                    else
                    {
                        valorTecnologia = "ADSL";
                    }
                }
                else if (!resultadoCobertura.DisponibilidadBANakedADSL && resultadoCobertura.DisponibilidadBAFibra)
                {
                    valorTecnologia = "FIBRA";
                }
                else if (resultadoCobertura.DisponibilidadBANakedADSL && resultadoCobertura.DisponibilidadBAFibra)
                {
                    if (resultadoCobertura.DisponibilidadBANakedVDSL)
                    {
                        valorTecnologia = "VDSL/FIBRA";
                    }
                    else
                    {
                        valorTecnologia = "ADSL/FIBRA";
                    }
                }
            }
            return valorTecnologia;
        }

        /// <summary>
        /// Autor: Migrado desde CCA
        /// Fecha: 10-Dic-2018
        /// convierte un objeto a Booleano
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ConvertObjectToBoolean<T>(this T value)
        {
            if (value != null)
            {
                return value.ToString().ToUpper().Equals("SI") || value.ToString().ToUpper().Equals("TRUE");

                int output;
                if (int.TryParse(value.ToString(), out output))
                    return Convert.ToBoolean(output);
                else
                    return Convert.ToBoolean(value);
            }
            else
                return false;
        }
    }
}
