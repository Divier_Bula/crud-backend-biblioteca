﻿using System;
using System.Collections.Generic;
using GeneradorInterfaces;
using CT.Common.Models;

namespace GeneradorEnrutador.BusinessLogic
{
    public class GeneradorOperacionBLBase : IOperacionBase
    {
        public RespuestaDTO EjecutarOperacion(String Clase, String Metodo, List<String> parametros)
        {
            RespuestaDTO mensaje = new RespuestaDTO();
            var assamble = Type.GetType(Clase);
            if (assamble != null)
            {
                var constructor = assamble.GetConstructor(Type.EmptyTypes);
                var instanciaObjeto = (GeneradorOperacionBLBase)constructor.Invoke(new object[] { });
                var iMetodo = assamble.GetMethod(Metodo);
                if(iMetodo != null)
                    mensaje = (RespuestaDTO)iMetodo.Invoke(instanciaObjeto, (parametros==null ? null : parametros.ToArray()));                
            }
            return mensaje;
        }
    }
}
