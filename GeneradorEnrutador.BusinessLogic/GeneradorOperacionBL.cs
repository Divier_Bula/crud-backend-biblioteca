﻿using System;
using System.Configuration;
using System.Windows.Input;
using GeneradorInterfaces;

namespace GeneradorEnrutador.BusinessLogic
{
    public class GeneradorOperacionBL
    {
        public static IOperacionBase InstanciarClase(string pClase)
        {
            try
            {
                var ensamblador = Type.GetType(pClase);
                if (ensamblador != null)
                {
                    ValideTypeIsCommand(ensamblador);
                    return Activator.CreateInstance(ensamblador) as IOperacionBase;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void ValideTypeIsCommand(Type type)
        {
            try
            {
                if (!typeof(IOperacionBase).IsAssignableFrom(type))
                {
                    throw new ConfigurationErrorsException(String.Format("La clase {0} no implementa la interfaz {1}", type.AssemblyQualifiedName, typeof(ICommand).AssemblyQualifiedName));
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
