﻿using System;
using System.Collections.Generic;
using CT.Common.Models;

namespace GeneradorInterfaces
{
    public interface IOperacionBase
    {
        RespuestaDTO EjecutarOperacion(String Clase, String Metodo, List<String> parametros); 
    }
}
