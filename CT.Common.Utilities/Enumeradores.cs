﻿
namespace CT.Common.Utilities
{
    public static class Enumeradores
    {
        public enum clases
        {
            [StringValue("GeneradorEnrutador.BusinessLogic.BusinessLogic.LibroBL")]
            LibroBL,
            [StringValue("GeneradorEnrutador.BusinessLogic.BusinessLogic.CategoriaBL")]
            CategoriaBL,
            [StringValue("GeneradorEnrutador.BusinessLogic.BusinessLogic.AutorBL")]
            AutorBL,
        }

    }
}
