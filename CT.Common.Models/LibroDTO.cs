﻿using System;

namespace CT.Common.Models
{
    public class LibroDTO 
    {
        public int Cod_Libro { get; set; }
        public int ISBN { get; set; }
        public String Nombre_Libro { get; set; }
        public int Cod_Autor { get; set; }
        public int Cod_Categoria { get; set; }
    }
}
