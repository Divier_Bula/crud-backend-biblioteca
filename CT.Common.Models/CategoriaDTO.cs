﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.Common.Models
{
    public class CategoriaDTO
    {
        public int Cod_Categoria { get; set; }
        public string Nombre_Categoria { get; set; }
        public string Descripcion_Categoria { get; set; }
    }
}
