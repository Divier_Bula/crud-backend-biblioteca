﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CT.Common.Models
{
    public class LoginRequestDTO
    {
        public string Username { get; set; }
        
        public string Password { get; set; }
    }
}