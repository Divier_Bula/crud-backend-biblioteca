﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.Common.Models
{
    public class RespuestaDTO
    {
        public string Codigo { get; set; }
        public string Mensaje { get; set; }
        public bool Exito { get; set; }
        public List<LibroDTO> Libros { get; set; }
        public List<CategoriaDTO> Categoria { get; set; }
        public List<AutorDTO> Autor { get; set; }
    }
}
