﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CT.Common.Models
{
    public class AutorDTO
    {
        public int Cod_Autor { get; set; }
        public string Nombre_Autor { get; set; }
        public string Apellido_Autor { get; set; }
    }
}
