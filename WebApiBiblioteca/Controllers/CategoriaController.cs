﻿using CT.Common.Models;
using CT.Common.Utilities;
using GeneradorEnrutador.BusinessLogic;
using System.Collections.Generic;
using System.Web.Http;

namespace WebApiBiblioteca.Controllers
{
    [Authorize]
    //[AllowAnonymous]
    [RoutePrefix("api/Categorias")]
    public class CategoriaController : ApiController
    {
        [HttpPost]
        [Route("Consultar_Categorias")]
        public IHttpActionResult Consultar_Categorias()
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.CategoriaBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.CategoriaBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, null);

            if (response.Exito && response.Libros.IsNullOrEmpty())
            {
                return Ok(new RespuestaDTO()
                {
                    Exito = true,
                    Mensaje = "No se encontro Información",
                });
            }
            else
                return Ok(response);
        }

        [HttpPost]
        [Route("Consultar_Categorias_X_Categoria")]
        public IHttpActionResult Consultar_Libros_X_Categoria(CategoriaDTO categoria)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.CategoriaBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.CategoriaBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { categoria.Nombre_Categoria });

            if (response.Exito && response.Libros.IsNullOrEmpty())
            {
                return Ok(new RespuestaDTO()
                {
                    Exito = true,
                    Mensaje = "No se encontro Información",
                });
            }
            else
                return Ok(response);
        }

        [HttpPost]
        [Route("Crear_Categoria")]
        public IHttpActionResult Crear_Categoria(CategoriaDTO Categoria)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.CategoriaBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.CategoriaBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { Categoria.Nombre_Categoria, Categoria.Descripcion_Categoria });
            return Ok(response);
        }

        [HttpPost]
        [Route("Actualizar_Categoria")]
        public IHttpActionResult Actualizar_Categoria(CategoriaDTO Categoria)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.CategoriaBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.CategoriaBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { Categoria.Nombre_Categoria, Categoria.Descripcion_Categoria, Categoria.Cod_Categoria.ToString() });
            return Ok(response);
        }

        [HttpPost]
        [Route("Eliminar_Categoria")]
        public IHttpActionResult Eliminar_Categoria(CategoriaDTO Categoria)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.CategoriaBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.CategoriaBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { Categoria.Cod_Categoria.ToString() });
            return Ok(response);
        }
    }
}
