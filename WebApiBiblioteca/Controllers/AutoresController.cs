﻿using GeneradorEnrutador.BusinessLogic;
using System.Collections.Generic;
using System.Web.Http;
using CT.Common.Models;
using CT.Common.Utilities;

namespace WebApi.Controllers
{
    [Authorize]
    //[AllowAnonymous]
    [RoutePrefix("api/autores")]
    public class AutoresController : ApiController
    {
        [HttpPost]
        [Route("Consultar_Autores")]
        public IHttpActionResult Consultar_Autores()
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.AutorBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.AutorBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, null);

            if (response.Exito && response.Libros.IsNullOrEmpty())
            {
                return Ok(new RespuestaDTO()
                {
                    Exito = true,
                    Mensaje = "No se encontro Información",
                });
            }
            else
                return Ok(response);
        }

        [HttpPost]
        [Route("Consultar_Libros_X_Autor")]
        public IHttpActionResult Consultar_Libros_X_Autor(AutorDTO autor)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.AutorBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.AutorBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { autor.Nombre_Autor });

            if (response.Exito && response.Libros.IsNullOrEmpty())
            {
                return Ok(new RespuestaDTO()
                {
                    Exito = true,
                    Mensaje = "No se encontro Información",
                });
            }
            else
                return Ok(response);
        }

        [HttpPost]
        [Route("Crear_Autor")]
        public IHttpActionResult Crear_autor(AutorDTO autor)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.AutorBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.AutorBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { autor.Nombre_Autor, autor.Apellido_Autor });
            return Ok(response);
        }

        [HttpPost]
        [Route("Actualizar_Autor")]
        public IHttpActionResult Actualizar_autor(AutorDTO autor)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.AutorBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.AutorBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { autor.Nombre_Autor, autor.Apellido_Autor, autor.Cod_Autor.ToString() });
            return Ok(response);
        }

        [HttpPost]
        [Route("Eliminar_Autor")]
        public IHttpActionResult Eliminar_autor(AutorDTO autor)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.AutorBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.AutorBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { autor.Cod_Autor.ToString() });
            return Ok(response);
        }
    }
}