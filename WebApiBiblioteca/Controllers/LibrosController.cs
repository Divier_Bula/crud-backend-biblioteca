﻿using GeneradorEnrutador.BusinessLogic;
using System.Collections.Generic;
using System.Web.Http;
using CT.Common.Models;
using CT.Common.Utilities;

namespace WebApiBiblioteca.Controllers
{
    [Authorize]
    //[AllowAnonymous]
    [RoutePrefix("api/libros")]
    public class LibrosController : ApiController
    {
        [HttpPost]
        [Route("Consultar_Libros")]
        public IHttpActionResult Consultar_Libros()
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.LibroBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.LibroBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, null);

            if (response.Exito && response.Libros.IsNullOrEmpty())
            {
                return Ok(new RespuestaDTO()
                {
                    Exito = true,
                    Mensaje = "No se encontro Información",
                });
            }
            else
                return Ok(response);
        }

        [HttpPost]
        [Route("Consultar_Libros_X_Nombre")]
        public IHttpActionResult Consultar_Libros_X_Nombre(LibroDTO libro)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.LibroBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.LibroBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { libro.Nombre_Libro });

            if (response.Exito && response.Libros.IsNullOrEmpty())
            {
                return Ok(new RespuestaDTO()
                {
                    Exito = true,
                    Mensaje = "No se encontro Información",
                });
            }
            else
                return Ok(response);
        }

        [HttpPost]
        [Route("Crear_Libro")]
        public IHttpActionResult Crear_Libro(LibroDTO libro)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.LibroBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.LibroBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { libro.Cod_Autor.ToString(), libro.ISBN.ToString(), libro.Nombre_Libro, libro.Cod_Categoria.ToString() });
            return Ok(response);
        }

        [HttpPost]
        [Route("Actualizar_Libro")]
        public IHttpActionResult Actualizar_Libro(LibroDTO libro)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.LibroBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.LibroBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { libro.Cod_Autor.ToString(), libro.ISBN.ToString(), libro.Nombre_Libro, libro.Cod_Libro.ToString() });
            return Ok(response);
        }

        [HttpPost]
        [Route("Eliminar_Libro")]
        public IHttpActionResult Eliminar_Libro(LibroDTO libro)
        {
            var IOperacion = GeneradorOperacionBL.InstanciarClase(Enumeradores.clases.LibroBL.ToStringAttribute());
            RespuestaDTO response = IOperacion.EjecutarOperacion(Enumeradores.clases.LibroBL.ToStringAttribute(), System.Reflection.MethodBase.GetCurrentMethod().Name, new List<string>() { libro.Cod_Libro.ToString() });
            return Ok(response);
        }
    }
}